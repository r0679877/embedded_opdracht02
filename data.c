#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DATAFILE "/var/www/html/myJsonFile.json"

// Decode the data that comes from the post
void unencode(char *src, char *last, char *dest)
{
    for (; src != last; src++, dest++)
        if (*src == '+')
        {
            *dest = ' ';
        }
        else if (*src == '%')
        {
            int code;
            if (sscanf(src + 1, "%2x", &code) != 1)
            {
                code = '?';
            }
            *dest = code;
            src += 2;
        }
        else
        {
            *dest = *src;
        }
    *++dest = '\0';
}

int main(void)
{
    char *lenstr;
    char input[100], data[100];
    long len;

    printf("%s%c%c\n", "Content-Type:text/html;charset=iso-8859-1", 13, 10);
    printf("<TITLE>Response</TITLE>\n");
    lenstr = getenv("CONTENT_LENGTH");
    if (lenstr == NULL || sscanf(lenstr, "%ld", &len) != 1 || len > 80)
        printf("<P>Error in invocation - wrong FORM probably.");
    else
    {
        fgets(input, len + 1, stdin);
        unencode(input, input + len, data);

        char *pch;
        char *stringArray[2];
        int i = 0;
        int j = 0;

        pch = strtok(data, "&=");
        while (pch != NULL)
        {
            if ((i % 2) == 1)
            {
                stringArray[j] = pch;
                j++;
            }
            pch = strtok(NULL, "&=");
            i++;
        }

	char timez[100];
  	time_t x= time(NULL);
  	struct tm tm = *localtime(&x);
 	sprintf(timez, "%d:%d", tm.tm_hour, tm.tm_min);

	printf("%s", timez);

	FILE *t = fopen(DATAFILE, "a");

        fseek(t, -1, SEEK_END);
        ftruncate(fileno(t), ftell(t));

	fprintf(t,",{\"key\":\"%s\",\"Value\":\"%s\",\"timestamp\":\"%s\"}]",stringArray[0],stringArray[1],timez);
        fclose(t);

        // Redirect to your previous page
        const char *redirect_page_format =
            "<html>\n"
            "<head>\n"
            "<meta http-equiv=\"REFRESH\"\n"
            "content=\"0;url=%s\">\n"
            "</head>\n"
            "</html>\n";
        printf(redirect_page_format, getenv("HTTP_REFERER"));
    }
    return 0;
}
