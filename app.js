$(document).ready(function () {
    loadData();
});

const table = document.querySelector('.table');

function loadData() {

    fetch('myJsonFile.json')
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            console.log(JSON.stringify(myJson));

            for (var i = 0; i < Object.keys(myJson).length; i++) {
                var tr = document.createElement("tr");
                var td0 = tr.insertCell(0);
                var td1 = tr.insertCell(1);
                var td2 = tr.insertCell(2);

                td0.innerHTML = i;
                td1.innerHTML = myJson[i].key;
                td2.innerHTML = myJson[i].Value;

                table.appendChild(tr);
            };
        });
};
